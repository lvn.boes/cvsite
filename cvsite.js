var currentTab = 0;
var tabsNL = ["#titlePage", "#overMijSec", "#itNlSec", "#jobNlSec", "#opleidingSec", "#vrijwilSec", "#contactSec"];
var tabsEN = ["#titlePage", "#aboutSec", "#itEnSec", "#jobEnSec", "#educationSec", "#voluntSec", "#contactSec"];


// Toggle languages

$(document).ready(function(){
  $("#nl").click(function(){
    $(".accContEl").css('display', 'none');
    $(".en").css('display', 'none');
    $(".nl").css('display', 'flex');
    $(tabsEN[currentTab]).css('display', 'none');
    $(tabsNL[currentTab]).css('display', 'flex');
    if (currentTab == 2) {
      $("#itAccNlSec11").css("display", "block");
      currentItTab = currentJobTab = 0;
    } else if (currentTab == 3) {
      $("#jobAccNlSec1").css("display", "block");
      currentItTab = currentJobTab = 0;
    }
  });
  $("#en").click(function(){
    $(".accContEl").css('display', 'none');
    $(".nl").css('display', 'none');
    $(".en").css('display', 'flex');
    $(tabsNL[currentTab]).css('display', 'none');
    $(tabsEN[currentTab]).css('display', 'flex');
    if (currentTab == 2) {
      $("#itAccEnSec1").css("display", "block");
      currentItTab = currentJobTab = 0;
    } else if (currentTab == 3) {
      $("#jobAccEnSec1").css("display", "block");
      currentItTab = currentJobTab = 0;
    }
  });


// Load titlepage

  $("#naamVeld").click(function(){
    if (currentTab != 0) {
      $(".accContEl").css('display', 'none');
      currentItTab = currentJobTab = -1;
      currentTab = 0;
      $(".jqBox").slideUp(600);
      $("#titlePage").slideDown(600);
    }
  });
  $("#nameField").click(function(){
    if (currentTab != 0) {
      $(".accContEl").css('display', 'none');
      currentItTab = currentJobTab = -1;
      currentTab = 0;
      $(".jqBox").slideUp(600);
      $("#titlePage").slideDown(600);
    }
  });


  // Load about section

  $("#overMijBut").click(function(){
    if (currentTab != 1) {
      $(".accContEl").css('display', 'none');
      currentItTab = currentJobTab = -1;
      currentTab = 1;
      $(".jqBox").slideUp(600);
      $("#overMijSec").slideDown(600);
    }
  });
  $("#aboutBut").click(function(){
    if (currentTab != 1) {
      $(".accContEl").css('display', 'none');
      currentItTab = currentJobTab = -1;
      currentTab = 1;
      $(".jqBox").slideUp(600);
      $("#aboutSec").slideDown(600);
    }
  });


  // Load it-skills section

  $("#itNlBut").click(function(){
    if (currentTab != 2) {
      currentTab = 2;
      $(".jqBox").slideUp(600);
      $("#itAccNlSec1").css("display", "flex");
      currentItTab = 0;
      $("#itNlSec").slideDown(600);
    }
  });
  $("#itEnBut").click(function(){
    if (currentTab != 2) {
      currentTab = 2;
      $(".jqBox").slideUp(600);
      $("#itAccEnSec1").css("display", "flex");
      currentItTab = 0;
      $("#itEnSec").slideDown(600);
    }
  });

// Load job section

  $("#jobNlBut").click(function(){
    if (currentTab != 3) {
      $(".accContEl").css('display', 'none');
      currentTab = 3;
      $(".jqBox").slideUp(600);
      $("#jobAccNlSec1").css("display", "block");
      currentJobTab = 0;
      $("#jobNlSec").slideDown(600);
    }
  });
  $("#jobEnBut").click(function(){
    if (currentTab != 3) {
      $(".accContEl").css('display', 'none');
      currentTab = 3;
      $(".jqBox").slideUp(600);
      $("#jobAccEnSec1").css("display", "block");
      currentJobTab = 0;
      $("#jobEnSec").slideDown(600);
    }
  });

// Load education section

  $("#opleidingBut").click(function(){
    if (currentTab != 4) {
      $(".accContEl").css('display', 'none');
      currentItTab = currentJobTab = -1;
      currentTab = 4;
      $(".jqBox").slideUp(600);
      $("#opleidingSec").slideDown(600);
    }
  });
  $("#educationBut").click(function(){
    if (currentTab != 4) {
      $(".accContEl").css('display', 'none');
      currentItTab = currentJobTab = -1;
      currentTab = 4;
      $(".jqBox").slideUp(600);
      $("#educationSec").slideDown(600);
    }
  });

// Load volunteering section

  $("#vrijwilBut").click(function(){
    if (currentTab != 5) {
      $(".accContEl").css('display', 'none');
      currentItTab = currentJobTab = -1;
      currentTab = 5;
      $(".jqBox").slideUp(600);
      $("#vrijwilSec").slideDown(600);
    }
  });
  $("#voluntBut").click(function(){
    if (currentTab != 5) {
      $(".accContEl").css('display', 'none');
      currentItTab = currentJobTab = -1;
      currentTab = 5;
      $(".jqBox").slideUp(600);
      $("#voluntSec").slideDown(600);
    }
  });

// Load contact section

$("#contactNlBut").click(function(){
  if (currentTab != 6) {
    $(".accContEl").css('display', 'none');
    currentItTab = currentJobTab = -1;
    currentTab = 6;
    $(".jqBox").slideUp(600);
    $("#contactSec").slideDown(600);
  }
});
$("#contactEnBut").click(function(){
  if (currentTab != 6) {
    $(".accContEl").css('display', 'none');
    currentItTab = currentJobTab = -1;
    currentTab = 6;
    $(".jqBox").slideUp(600);
    $("#contactSec").slideDown(600);
  }
});

// It Accordeon

var currentItTab = -1;
var itTabsNl = ["#itAccNlSec1", "#itAccNlSec2", "#itAccNlSec3", "#itAccNlSec4"];
var itTabsEn = ["#itAccEnSec1", "#itAccEnSec2", "#itAccEnSec3", "#itAccEnSec4"];

  $("#itAccNlBut1").click(function(){
    if (currentItTab != 0) {
      $(itTabsNl[currentItTab]).slideUp(600);
      currentItTab = 0;
    } else {
      currentItTab = -1;
    }
    $("#itAccNlSec1").slideToggle(600);
  });
  $("#itAccNlBut2").click(function(){
    if (currentItTab != 1) {
      $(itTabsNl[currentItTab]).slideUp(600);
      currentItTab = 1;
    } else {
      currentItTab = -1;
    }
    $("#itAccNlSec2").slideToggle(600);
  });
  $("#itAccNlBut3").click(function(){
    if (currentItTab != 2) {
      $(itTabsNl[currentItTab]).slideUp(600);
      currentItTab = 2;
    } else {
      currentItTab = -1;
    }
    $("#itAccNlSec3").slideToggle(600);
  });
  $("#itAccNlBut4").click(function(){
    if (currentItTab != 3) {
      $(itTabsNl[currentItTab]).slideUp(600);
      currentItTab = 3;
    } else {
      currentItTab = -1;
    }
    $("#itAccNlSec4").slideToggle(600);
  });

  $("#itAccEnBut1").click(function(){
    if (currentItTab != 0) {
      $(itTabsEn[currentItTab]).slideUp(600);
      currentItTab = 0;
    } else {
      currentItTab = -1;
    }
    $("#itAccEnSec1").slideToggle(600);
  });
  $("#itAccEnBut2").click(function(){
    if (currentItTab != 1) {
      $(itTabsEn[currentItTab]).slideUp(600);
      currentItTab = 1;
    } else {
      currentItTab = -1;
    }
    $("#itAccEnSec2").slideToggle(600);
  });
  $("#itAccEnBut3").click(function(){
    if (currentItTab != 2) {
      $(itTabsEn[currentItTab]).slideUp(600);
      currentItTab = 2;
    } else {
      currentItTab = -1;
    }
    $("#itAccEnSec3").slideToggle(600);
  });
  $("#itAccEnBut4").click(function(){
    if (currentItTab != 3) {
      $(itTabsEn[currentItTab]).slideUp(600);
      currentItTab = 3;
    } else {
      currentItTab = -1;
    }
    $("#itAccEnSec4").slideToggle(600);
  });

// Job accordeon

var currentJobTab = -1;
var jobTabsNl = ["#jobAccNlSec1", "#jobAccNlSec2", "#jobAccNlSec3", "#jobAccNlSec4"];
var jobTabsEn = ["#jobAccEnSec1", "#jobAccEnSec2", "#jobAccEnSec3", "#jobAccEnSec4"];

  $("#jobAccNlBut1").click(function(){
    if (currentJobTab != 0) {
      $(jobTabsNl[currentJobTab]).slideUp(600);
      currentJobTab = 0;
    } else {
      currentJobTab = -1;
    }
    $("#jobAccNlSec1").slideToggle(600);
  });
  $("#jobAccNlBut2").click(function(){
    if (currentJobTab != 1) {
      $(jobTabsNl[currentJobTab]).slideUp(600);
      currentJobTab = 1;
    } else {
      currentJobTab = -1;
    }
    $("#jobAccNlSec2").slideToggle(600);
  });
  $("#jobAccNlBut3").click(function(){
    if (currentJobTab != 2) {
      $(jobTabsNl[currentJobTab]).slideUp(600);
      currentJobTab = 2;
    } else {
      currentJobTab = -1;
    }
    $("#jobAccNlSec3").slideToggle(600);
  });
  $("#jobAccNlBut4").click(function(){
    if (currentJobTab != 3) {
      $(jobTabsNl[currentJobTab]).slideUp(600);
      currentJobTab = 3;
    } else {
      currentJobTab = -1;
    }
    $("#jobAccNlSec4").slideToggle(600);
  });

  $("#jobAccEnBut1").click(function(){
    if (currentJobTab != 0) {
      $(jobTabsEn[currentJobTab]).slideUp(600);
      currentJobTab = 0;
    } else {
      currentJobTab = -1;
    }
    $("#jobAccEnSec1").slideToggle(600);
  });
  $("#jobAccEnBut2").click(function(){
    if (currentJobTab != 1) {
      $(jobTabsEn[currentJobTab]).slideUp(600);
      currentJobTab = 1;
    } else {
      currentJobTab = -1;
    }
    $("#jobAccEnSec2").slideToggle(600);
  });
  $("#jobAccEnBut3").click(function(){
    if (currentJobTab != 2) {
      $(jobTabsEn[currentJobTab]).slideUp(600);
      currentJobTab = 2;
    } else {
      currentJobTab = -1;
    }
    $("#jobAccEnSec3").slideToggle(600);
  });
  $("#jobAccEnBut4").click(function(){
    if (currentJobTab != 3) {
      $(jobTabsEn[currentJobTab]).slideUp(600);
      currentJobTab = 3;
    } else {
      currentJobTab = -1;
    }
    $("#jobAccEnSec4").slideToggle(600);
  });
});
